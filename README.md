ofxVideoSource 
==============

ofxVideoSource is a convenient wrapper around video source classes in
openFrameworks, allowing for easy exchange of video sources in
applications.

Build
-----

Just copy the source repository to your openFrameworks `addon` folder
and add "ofxVideoSource" to your `addons.make` file.

Usage
-----

The ofxVideoSource class mostly mimics the API of ofVideoGrabber:

    // in your app's class definition
    ofxVideoSource source;

    // in your app's setup() method
    source.setup ("grabber:0", 640, 480, 30);

    
    // in your app's update() method
    source.update()
    if (source.isFrameNew()) {
    	ofPixels &pixels = source.getPixels();

    	// do stuff with pixels

    }

    // in your app's draw() method
    source.draw (0, 0, 200, 200);

the only difference is that the setup() method uses a simple DSL to
choose which ofVideo class or gstreamer template to use. The DSL
consists of a source type followed by a ':' and a list of parameters
for that source type also separated by ':'. so `grabber:0` instantiates
the system camera with index 0.

Currently available types are:

 * `grabber`: use the openFrameworks video grabber to open a system camera. 
    Has a single parameter that can be the system name or the index 
    of the camera to open:
    * `grabber:0`
    * `grabber:PS3 USB Camera`
 * `axis`: open an Axis IP camera stream. Parameters are codec and 
    hostname/ip address of the camera:
    * `axis:mjpeg:192.168.1.90`
    * `axis:h264:axis1.local` 
 * `pipeline` : open an arbitrary gstreamer pipeline:
   * `pipeline:testvideosrc ! testvideosink`
 * `image` : use a static image in the `data/` folder as a video stream. 
   Currently only PNG files are supported:
   * `image:background.png`
 * `video` : use a video file in the `data/` folder as a video source.
   Currently only Ogg Theora files are supported:
   * `video:session_recording.ogg`

Development
-----------

This addon evolved from practical needs, so some features were never
needed and some cruft has accumulated over time. To make this addon
generally useful, future iterations might include:

 * make ofxVideoSource be an abstract interface for a video source
 * provide a factory method to build instances of ofxVideoSource
 * extend source definition DSL to use key-value parameters
 * replace ad-hoc DSL parsing with a parser that outputs a vector/map of parameters 
 * implement a raspicam source
 * get rid of axis camera source
 * allow subformat specification for file types (video:ogv:,
video:m4v:, video:mov:, etc), or do a typefind based on file extension
or magic bytes (check wether we can leverage gstreamers typefind or
playbin for this) 
 * bypass ofGST* and use gstreamer directly (this
would require re-implementing parts of ofGSTVideo{Player,Grabber})
