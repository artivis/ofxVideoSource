#include "ofxVideoSource.h"

#include "ofGstVideoGrabber.h"
#include "ofTexture.h"
#include "ofUtils.h"

enum video_source_type {
  VIDEO_SOURCE_NONE,
  VIDEO_SOURCE_GRABBER,
  VIDEO_SOURCE_AXIS,
  VIDEO_SOURCE_PIPELINE,
  VIDEO_SOURCE_IMAGE,
  VIDEO_SOURCE_FILE
};

struct video_source_implementation {
  ofGstVideoGrabber source;
  video_source_type type;
  ofTexture output;
  unsigned int fps;

  video_source_implementation():
    source(),
    type (VIDEO_SOURCE_NONE),
    output(),
    fps(0)
    { }
};

//
// Axis gstremer pipeline templates
//

const std::string AXIS_HTTP_SOURCE = "souphttpsrc do-timestamp=true is-live=true location=http://";
const std::string AXIS_RTSP_SOURCE = "rtspsrc location=rtsp://";
const std::string AXIS_MPEG4_PATH = "/mpeg4/media.amp";
const std::string AXIS_MJPEG_PATH = "/mjpg/video.mjpg";
const std::string AXIS_MPEG4_PIPELINE = " ! rtpmp4vdepay ! ffdec_mpeg4 ! ffmpegcolorspace";
const std::string AXIS_MJPEG_PIPELINE = " ! jpegdec ! ffmpegcolorspace";

// FIXME: MJPEG handling is different in gstreamer-0.10 and gstreamer 1.0
// 1.0 (ubuntu 12.10):
// const std::string MAXIS_MJPEG_PIPELINE = " ! multipartdemux ! avdec_mjpeg";
// 0.10 (debian wheezy)

ofxVideoSource::ofxVideoSource():
  mpi (new video_source_implementation)
  { }

ofxVideoSource::~ofxVideoSource()
{
  if (mpi->type != VIDEO_SOURCE_NONE) {
    (mpi->source).close();
  }
  delete mpi;
}

void
ofxVideoSource::setup (
  const std::string & source,
  unsigned int width,
  unsigned int height,
  unsigned int fps)
{
  mpi->fps = fps;

  // Splice the source parameter
  unsigned int sep_pos = source.find (':');
  std::string source_type = source.substr (0, sep_pos);
  std::string source_params = source.substr (sep_pos + 1);

  ofGstVideoUtils * p_gst_pipeline = (mpi->source).getGstVideoUtils();

  if (source_type == "grabber") {
    (mpi->source).listDevices();
    (mpi->source).setDeviceID (ofToInt (source_params));
    if ((mpi->source).setup (width, height)) {
      (mpi->source).setDesiredFrameRate (fps);
      mpi->type = VIDEO_SOURCE_GRABBER;
    }
  }
  else if (source_type == "axis") {
    (mpi->type) = VIDEO_SOURCE_AXIS;
    sep_pos = source_params.find (':');
    std::string axis_codec = source_params.substr (0, sep_pos);
    std::string axis_location = source_params.substr (sep_pos + 1);
    std::string axis_pipeline = AXIS_HTTP_SOURCE + axis_location;

    if (axis_codec == "mjpeg") {
      axis_pipeline += AXIS_MJPEG_PATH + AXIS_MJPEG_PIPELINE;
      p_gst_pipeline->setPipeline (axis_pipeline, OF_PIXELS_RGB, false, width, height);
    }
    else if (axis_codec == "h264"){
      axis_pipeline += AXIS_MPEG4_PATH + AXIS_MPEG4_PIPELINE;
      p_gst_pipeline->setPipeline (axis_pipeline, OF_PIXELS_RGB, false, width, height);
    }
    else {
      mpi->type = VIDEO_SOURCE_NONE;
      mpi->fps = 0;
    }

    p_gst_pipeline->startPipeline();
    p_gst_pipeline->play();
  }
  else if (source_type == "pipeline") {
    mpi->type = VIDEO_SOURCE_PIPELINE;
    p_gst_pipeline->setPipeline (source_params, OF_PIXELS_RGB, false, width, height);
    p_gst_pipeline->startPipeline();
    p_gst_pipeline->play();
  }
  else if (source_type == "image") {
    std::string image_path = ofToDataPath ("image/" + source_params, false);
    std::string image_pipeline = "filesrc location=" + image_path + " ! pngdec ! imagefreeze ! autovideoconvert ! videoscale";
    p_gst_pipeline->setPipeline (image_pipeline, OF_PIXELS_RGB, false, width, height);
    p_gst_pipeline->startPipeline();
    p_gst_pipeline->play();
  }
  else if (source_type == "video") {
	mpi->type = VIDEO_SOURCE_FILE;
	std::string video_path = ofToDataPath (source_params, false);
	std::string video_pipeline = "filesrc location=" + video_path + " ! oggdemux  ! theoradec ! ffmpegcolorspace";
	p_gst_pipeline->setPipeline (video_pipeline, OF_PIXELS_RGB, false, width, height);
        p_gst_pipeline->startPipeline();
	p_gst_pipeline->play();
      }

  (mpi->output).allocate (width, height, GL_RGB);
}

void
ofxVideoSource::update()
{
  if (mpi->type == VIDEO_SOURCE_NONE) {
    return;
  }

  (mpi->source).update();
  (mpi->output).loadData ((mpi->source).getPixels());
}

void
ofxVideoSource::draw (int x, int y, int width, int height)
{
  (mpi->output).draw (x, y, width, height);
}

bool
ofxVideoSource::isFrameNew()
{
  return (mpi->source).isFrameNew();
}

float
ofxVideoSource::getWidth()
{
  return (mpi->source).getWidth();
}

float
ofxVideoSource::getHeight()
{
  return (mpi->source).getHeight();
}

unsigned int
ofxVideoSource::getFPS()
{
  return mpi->fps;
}

ofPixels &
ofxVideoSource::getPixels()
{
  return (mpi->source).getPixels();
}
