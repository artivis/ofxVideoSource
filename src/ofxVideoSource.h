#ifndef OFX_VIDEOSOURCE_H
#define OFX_VIDEOSOURCE_H

#include "ofPixels.h"

#include <string>

struct video_source_implementation;

class ofxVideoSource {

  public:

  ofxVideoSource();
  ~ofxVideoSource();

  void setup (const std::string &source, unsigned int width,
    unsigned int height, unsigned int fps);

  void update();
  void draw (int x, int y, int width, int height);
  bool isFrameNew();
  float getWidth();
  float getHeight();
  unsigned int getFPS();

  ofPixels & getPixels();

  private:

    video_source_implementation *mpi;
};

#endif // OFX_VIDEOSOURCE_H
